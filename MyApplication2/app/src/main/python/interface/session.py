class Session():
    id = None
    algorithm = None

    def __init__(self, id, algorithm) -> None:
        self.id = id
        self.algorithm = algorithm